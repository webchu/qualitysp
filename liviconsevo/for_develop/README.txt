IMPORTANT!

(1)
Please don't forget to edit the "pathToFolder" option in the "LivIconsEvo.defaults.js" file from the "for_develop > LivIconsEvo > js" folder.
If you upload the "LivIconsEvo" folder to the root site's directory on your server, the value of "pathToFolder" option should be either '/LivIconsEvo/svg/' or 'http://www.your_site.com/LivIconsEvo/svg/' or 'https://www.your_site.com/LivIconsEvo/svg/' if you use an encryption. 

(2) 
LivIcons Evolution script uses an AJAX technology for the working. Thus no icons will appear on your page if that page is opened directly from your HDD in a browser (like "file:///C:/path/to/your_page.html").
Use this script on a working local (like XAMP, LAMP, etc.) or a global web-server, as you work as usual, for example, with PHP files.